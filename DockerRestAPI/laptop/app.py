"""
Author: Shikun Lin
UO CIS322 18F

This is a flask server which can calculate the control times.
And save the data to database. Then, send data from database to
front end and print on web page. Also, it we can get differenr format
data from the database

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Api, Resource, reqparse
import logging



###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

#Holder for the data whcih need to be saved
data_saved = ""

###
# Pages
###

#parser = reqparse.RequestParser()
#parser.add_argument('top')


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    #Get the data from front end
    km = request.args.get('km', 999, type=float)
    beg_date = request.args.get("beg_date",type = str)
    beg_time = request.args.get("beg_time",type = str)
    distance = request.args.get("distance",type = int)
    location = request.args.get("location",type=str)
    # Create a arrow object
    date_time = arrow.utcnow()
    date = arrow.get(beg_date+" "+beg_time,'YYYY-MM-DD HH:mm').isoformat()
    #Set the value based on front end data
    date_time = arrow.get(date)
    #Set the time zone
    date_time=date_time.replace(tzinfo='US/Pacific')
    date_time = date_time.isoformat()


    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, date_time)
    close_time = acp_times.close_time(km, distance, date_time)
    result = {"open": open_time, "close": close_time}


    #Add the new data to the holder in correct in html syntax
    global data_saved

    data_saved = data_saved + 'Location: '+ location + '<br/>'\
        +'     Brevet_Distance: ' + str(distance) + '<br/>'\
        + '     KM: ' + str(km) + '<br/>'\
        + '     Open: ' + open_time + '<br/>'\
        + '     Close: '  + close_time + " " +'<br/>' + '<br/>'
    return flask.jsonify(result=result)

#Send the json with data into database
@app.route('/_submit', methods=['POST'])
def new():
    global data_saved

    #make an Json for data
    data = {'data':data_saved}
    #insert to database
    if(len(data["data"])>0):
        db.tododb.insert_one(data)
    #reset the holder to empyty string
    data_saved = ""
    return flask.render_template('calc.html')

#Display the data which saved in database on webpage
@app.route('/_display',methods=['POST'])
def display():
    #Holder for the data need to transfer to fornt end
    items = [{"data":""}]

    #If the database is not empyty, then get the data from database
    if db.tododb.count() == 0:
        items = [{"data":""}]
    else:
        _items = db.tododb.find()
        items = [item for item in _items]
        #delete the Mongodb id from the data which send to front end
        for item in items:
            del item['_id']
    #rendering todo.html with data
    return render_template('todo.html', items=items)

######Helper functions to help get correct format data######

#Get all open and close as list
def get_all_list(items):
    res_list = []
    res = [item.split() for item in items]

    lens = len(res)
    #replace all <br/> with ""
    for i in range(lens):
        for j in range(len(res[i])):
            res[i][j] = res[i][j].replace('<br/>','')

    #get open times and close times
    for i in range(len(res)):
        for j in range(len(res[i])):
            if res[i][j] == "Open:" or res[i][j] == "Close:":
                 res_list.append(res[i][j]+res[i][j+1])
                 j+=1
    return res_list

#Get open time only from all list
def get_open_list(all_list):
    open_list = []
    for item in all_list:
        if "Open:" in item:
            open_list.append(item)
    return open_list

#Get close times only from all list
def get_close_list(all_list):
    close_list = []
    for item in all_list:
        if "Close:" in item:
            close_list.append(item)
    return close_list

#It can make open times and close in csv format
def get_all_lsit_csv(all_list):
    csv_list = []
    for i in range(len(all_list)):
        temp = all_list[i]
        if "Open:" in temp or "Close:" in temp:
            temp = temp.split(":")
            csv_list.append(temp[0]+", "+temp[1]+":"+temp[2]+":"+temp[3]+":"+temp[4]+"<br/>")
    return csv_list

#It can make Open time and close time in json format
def get_all_list_json(all_list):
    json_list = []

    for i in range(len(all_list)):
        temp = all_list[i]
        if "Open:" in temp:
            temp = temp.split(":")
            temp2 = all_list[i+1].split(":")
            #make a json for every pair of open time and close time
            json_list.append({
                temp[0]:temp[1]+":"+temp[2]+":"+temp[3]+":"+temp[4],
                temp2[0]:temp2[1]+":"+temp2[2]+":"+temp2[3]+":"+temp2[4]
                })
            i +=1
    return json_list

#It make open time or close time in json format
def get_open_close_list_json(lst):
    json_list = []

    for i in range(len(lst)):
        temp = lst[i]
        if "Open:" in temp or "Close:" in temp:
            temp = temp.split(":")
            json_list.append({
                temp[0]:temp[1]+":"+temp[2]+":"+temp[3]+":"+temp[4]
                })
    return json_list


#sort key for top argument
def get_open_time(open_time_json):
    return (open_time_json["Open"])
def get_close_time(close_time_json):
    return (close_time_json["Close"])

#listAll Api
class ListAll(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        return all_list

#listOpenOnly Api
class ListAllOpenOnly(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        open_list = get_open_list(all_list)
        return open_list

#listCloseOnly APi
class ListAllCloseOnly(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        close_list = get_close_list(all_list)

        return close_list



#listAll/json or listAll/csv Api
class ListAllFormatted(Resource):
    def get(self,formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        if formatted == "csv":
            csv_string = get_all_lsit_csv(all_list)
            return csv_string
        elif formatted == "json":
            json_list = get_all_list_json(all_list)
            return json_list

#listOpenOnly/json and listOpenOnly/csv Api
class ListOpenOnlyFormatted(Resource):
    def get(self,formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        open_list = get_open_list(all_list)
        top = request.args.get("top", type = str)
        if formatted == "csv":
            open_csv = []
            #Check for top is null or not
            if top:
                open_json = get_open_close_list_json(open_list)
                open_json.sort(key = get_open_time)
                res = open_json[:int(top)]
                for it in res:
                    open_csv.append("Open, "+it["Open"] +"<br/>")
                return open_csv
            else:
                open_csv = get_all_lsit_csv(open_list)
                return open_csv
        elif formatted == "json":
            open_json = get_open_close_list_json(open_list)
            if top:
                open_json.sort(key = get_open_time)
                res = open_json[:int(top)]
                return res
            return open_json

#listCloseOnly/json and listCloseOnly/csv Api
class ListCloseOnlyFormatted(Resource):
    def get(self, formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]

        all_list = get_all_list(items)
        close_list = get_close_list(all_list)
        top = request.args.get("top", type = str)
        if formatted == "csv":
            close_csv = []
            #Check top argument is null or not
            if top:
                close_json = get_open_close_list_json(close_list)
                close_json.sort(key = get_close_time)
                res = close_json[:int(top)]
                for it in res:
                    close_csv.append("Close, "+it["Close"]+"<br/>")
                return close_csv
            else:
                close_csv = get_all_lsit_csv(close_list)
                return close_csv
        elif formatted == "json":
            close_json = get_open_close_list_json(close_list)
            if top:
                close_json.sort(key = get_close_time)
                res = close_json[:int(top)]
                return res
            return close_json



#############ADD The API TO Resource#######################
api.add_resource(ListAll,'/listAll')
api.add_resource(ListAllOpenOnly,'/listOpenOnly')
api.add_resource(ListAllCloseOnly,'/listCloseOnly')
api.add_resource(ListAllFormatted,'/listAll/<formatted>')
api.add_resource(ListOpenOnlyFormatted,'/listOpenOnly/<formatted>')
api.add_resource(ListCloseOnlyFormatted,'/listCloseOnly/<formatted>')

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
