<html>
    <head>
        <title>CIS 322 ACP CAl</title>
    </head>

    <body>
        <h1>List of Open Times and Close Times In Json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll/json');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo "<li>Open Time: $times->Open</li>";
                echo "<li>Close Time: $times->Close</li>";
                echo "==============================================<br/>";
            }
            ?>
        </ul>
        <h1>List of Open Times and Close Times In CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll/csv');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo $times;
            }
            ?>
        </ul>
        <h1>List of Open Times In Json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listOpenOnly/json');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo "<li>Open Time: $times->Open</li>";
                echo "==============================================<br/>";
            }
            ?>
        </ul>
        <h1>List of Open Times In CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listOpenOnly/csv');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo $times;
            }
            ?>
        </ul>
        <h1>List of Close Times In Json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/json');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo "<li>Close Time: $times->Close</li>";
                echo "==============================================<br/>";
            }
            ?>
        </ul>
        <h1>List of Colse Times In CSV</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/csv');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo $times;
            }
            ?>
        </ul>


        <h1>List of Open Times In Json With top = 3</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listOpenOnly/json?top=3');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo "<li>Open Time: $times->Open</li>";
                echo "==============================================<br/>";
            }
            ?>
        </ul>
        <h1>List of Open Times In CSV With top = 3</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listOpenOnly/csv?top=3');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo $times;
            }
            ?>
        </ul>
        <h1>List of Close Times In Json With top = 3</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/json?top=3');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo "<li>Close Time: $times->Close</li>";
                echo "==============================================<br/>";
            }
            ?>
        </ul>
        <h1>List of Colse Times In CSV With top = 3</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/csv?top=3');
            $obj = json_decode($json);
            foreach ($obj as $times) {
                echo $times;
            }
            ?>
        </ul>


    </body>
</html>
