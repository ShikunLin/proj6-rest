# Project 6: Brevet time calculator service

Author: Shikun Lin  
Contact address: shikunl@uoregon.edu  

## What is in this repository

You have a minimal implementation of Docker compose in DockerRestAPI folder, using which you can create REST API-based services (as demonstrated in class). 

## Description  

This project have three major parts. First, a flask server, which is a ACP Caculator. This should be run on port which made on credentials.ini file. Second, a mongodb, which can save the caculated data into database. Third, a PHP consumer server, which can print an output for all of API result. For example, you open "http://127.0.0.1:5001" to see all results of the API.  
  

 * "http://127.0.0.1:5000/listAll" should return all open and close times in the database
 * "http://127.0.0.1:5000/listOpenOnly" should return open times only
 * "http://127.0.0.1:5000/listCloseOnly" should return close times only
 * "http://127.0.0.1:5000/listAll/json" should return all open and close times in JSON format
 * "http://127.0.0.1:5000/listOpenOnly/json" should return open times only in JSON format
 * "http://127.0.0.1:5000/listCloseOnly/json" should return close times only in JSON format
 * "http://127.0.0.1:5000/listAll/csv" should return open and close times in CSV format
 * "http://127.0.0.1:5000/listOpenOnly/csv" should return open times only in CSV format
 * "http://127.0.0.1:5000/listCloseOnly/csv" should return close times only in CSV format
 * "http://127.0.0.1:5000/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
 * "http://127.0.0.1:5000/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format

## How to Run  

you need a credentials.ini file inside ./DockerRestAPI/laptop  

1. cd to ./DockerRestAPI 
2. run "docker-compose up"